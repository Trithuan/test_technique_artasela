const { urlencoded } = require('express');
const express = require('express');
const controller = require('./controllers/controller');
const ip = require('ip');

const hostname = ip.address();

const app = express();
app.set('view engine', 'ejs');

controller(app);


app.listen('3000','localhost', ()=> {
    console.log('Server started on port 3000');
});

